from urlparse import urlparse
import pyimgur as imgur
import sys
import os


LINK    = 'l'
SUB     = 's'
USER    = 'u'
IMGUR_CLIENT_ID = 'b8b777916d21dcc'
im = imgur.Imgur(IMGUR_CLIENT_ID)


config  = {}
debug   = False
mode    = None
data    = None
SUBDIR  = '/unsorted'


def check_folder_exists(path):
    try:
        os.stat(path)
    except:
        os.mkdir(path)


def get_config(filename='config.py'):
    config_path = os.path.join(os.path.dirname(__file__), filename)
    temp_dict = {}
    with open(config_path, 'r') as config_file:
        exec(config_file.read(), temp_dict)
        config_file.close()
    return temp_dict


def download_imgur(image_id, subdir=None):
    img = im.get_image(image_id)
    dlpath = DL_ROOT+SUBDIR
    check_folder_exists(dlpath)
    if subdir is not None:
        dlpath += '/' + subdir
        check_folder_exists(dlpath)
    try:
        newdir = img.download(path=dlpath)
        print "DOWNLOADED ___ " + newdir
    except Exception, ex:
        print ex


def download_link(link):
    ''' Downloads the given link
        Return codes:
            0 : File downloaded correctly
            1 : Generic/unknown error
            2 : Error parsing URL
            3 : Invalid domain (for now, anything other than imgur)
    '''
    uri = urlparse(link)
    if uri[0] == '':
        uri = urlparse('http://' + link)
        if uri[0] == '':
            return 2
    if 'imgur.com' in uri[1]:
        obj = im.get_at_url(link)
        if '#' in link:
            link = link.split('#')[0]
        if type(obj) is imgur.Album:
            for img in obj.images:
                download_imgur(img.id, obj.title)
        elif type(obj) is imgur.Image:
            download_imgur(obj.id)
        else:
            return 1
    else:
        return 3


args            = sys.argv
config          = get_config()
DL_ROOT         = config['download_root']
REDDIT_AUTH     = config['reddit_auth']
check_folder_exists(DL_ROOT)


# Die if too few arguments are given
if len(args) < 3:
    print "Too few arguments"
    sys.exit()


# Determine the sraper mode
if '-l' in args:
    mode = LINK
elif '-u' in args:
    mode = USER
elif '-s' in args:
    mode = SUB


# Enable debug mode if flag exists
if '-d' in args:
    debug = True


# Retrieve the data from arguments
data = args[-1]


if mode == LINK:
    download_link(data)

elif mode == SUB:
    None

elif mode == USER:
    None
